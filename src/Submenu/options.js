import React,{useState, useEffect} from 'react';
import styled from 'styled-components';
import check from '../images/ok.png'

const Container = styled.div`
   margin-left:15px;
   margin-top:15px;
   display:flex;
   display-direction:row;
   justify-content:space-between;
   margin-right:25px;
   align-items:center
`;

const Title = styled.p`
    margin:0px;
    color: white;
    font-weight:500;
`

export const Circle = styled.div`
    border-radius:50%;
    border:1.8px solid white;
    height:35px;
    width:35px;
    cursor:pointer;
    background-color: ${props => props.visible ? 'white':'transparent'};
`
export const Icon = styled.img`
    height: 35px;
    width: 35px;
    border-radius: 50px;
    visibility: ${props => props.visible ? 'visible':'hidden'};
`


const App = ({title,state}) =>{

    const [visible, setVisible] = useState(false);
    useEffect(() => {
        setVisible(!state);
    },[state])

    const handleChange = () => {
        setVisible(!visible)
    };

    return(
        <Container>
            <Title>
                {title}
            </Title>
            <Circle visible={visible} onClick={handleChange}>
                <Icon src={check} visible={visible}></Icon>
            </Circle>
        </Container>
    );
}

export default App;