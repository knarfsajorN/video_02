import React from 'react';
import styled from 'styled-components';
import Options,{Circle,Icon} from './options';

import check from '../images/ok.png'
import cancel from '../images/error.png'


const Container = styled.div`
    background-color:#21d0d0;
    border-radius:15px;
    height:200px;
    width: 400px;
    padding-top:15px;

    //opacity:0.8;
`;

const Footer = styled.div`
    display:flex;
    justify-content:center;
    align-items:center;
    flex-direction:row;

`

const App = () =>{
    const options = [
        {title: "PRICE LOW TO HIGHT",state:false},
        {title: "PRICE HIGH TO LOW",state:false},
        {title: "POPULARITY",state:true},
    ]
    return(
        <Container>
            { options.map((v,i) => <Options key={i}{...v}/>)}
            <Footer>
                {
                    [cancel,check,].map((v,i) => {
                        return (
                            <Circle visible={true} style={{ marginLeft:15,height:60,width:60}}>
                                <Icon visible={true} src={v} style={{height:60,width:60}}></Icon>
                            </Circle>
                        )
                    })
                }
               
            </Footer>
        </Container>
    );
}

export default App;